from fastapi import FastAPI

from .api.v1 import memes
from .database import engine, Base

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(memes.router, tags=["memes"])
