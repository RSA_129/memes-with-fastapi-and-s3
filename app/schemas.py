import json
from typing import Optional

from pydantic import BaseModel, model_validator


class MemeBase(BaseModel):
    """
    Base schema for pydantic meme objects.
    """
    title: str
    description: Optional[str] = None


class MemeCreate(MemeBase):
    """
    Schema for creating a new meme.
    Validates if the input is a JSON string and converts it to a dictionary.
    """

    @model_validator(mode='before')
    @classmethod
    def validate_to_json(cls, value):
        if isinstance(value, str):
            return cls(**json.loads(value))
        return value


class MemeUpdate(BaseModel):
    """
    Schema for updating an existing meme.
    All fields are optional.
    """
    title: Optional[str] = None
    description: Optional[str] = None


class MemeOut(MemeBase):
    """
    Schema for returning a meme object to user.
    """
    id: int
    image_url: str

    class Config:
        from_attributes = True
