import pathlib
from typing import List

from fastapi import APIRouter, Depends, HTTPException, UploadFile
from sqlalchemy.orm import Session

from ... import config
from ...crud import get_meme_db, get_memes_db, create_meme_db, update_meme_db, delete_meme_db
from ...dependencies import get_db
from ...minio_client import upload_file_to_minio
from ...schemas import MemeOut, MemeCreate, MemeUpdate

_path_file = pathlib.Path(__file__)
PREFIX = f'/{_path_file.parent.parent.name}/{_path_file.parent.name}/{_path_file.stem}'

router = APIRouter(
    prefix=PREFIX
)


@router.get("", response_model=List[MemeOut])
def read_memes(offset: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    """
    Retrieve a list of memes.

    Args:
        offset: Number of memes to skip.
        limit: Maximum number of memes to return.
        db: Database session.

    Returns:
        List of memes.
    """
    memes = get_memes_db(db, offset=offset, limit=limit)
    return memes


@router.get("/{meme_id}", response_model=MemeOut)
def read_meme(meme_id: int, db: Session = Depends(get_db)):
    """
    Retrieve a meme by ID.

    Args:
        meme_id: ID of the meme to retrieve.
        db: Database session.

    Returns:
        Meme object.

    Raises:
        HTTPException: If the meme is not found.
    """
    db_meme = get_meme_db(db, meme_id=meme_id)
    if db_meme is None:
        raise HTTPException(status_code=404, detail=f"Meme with id {meme_id} not found")
    return db_meme


@router.post("", response_model=MemeOut)
def create_meme(meme: MemeCreate, file: UploadFile, db: Session = Depends(get_db)):
    """
    Create a new meme.

    Args:
        meme: Meme data.
        file: Image file.
        db: Database session.

    Returns:
        Created meme object.

    Raises:
        HTTPException: If no file is uploaded, if the file is not an image, if the file exceeds size limit,
                       or if there is an error uploading the file to S3 storage.
    """
    if not file:
        raise HTTPException(status_code=400, detail="No file uploaded")
    if file.content_type not in config.ALLOWED_IMAGE_TYPES:
        raise HTTPException(
            status_code=400,
            detail=f"Invalid file type. Only image files are allowed: {', '.join(config.ALLOWED_IMAGE_TYPES)}"
        )
    if file.size > config.MAX_FILE_SIZE:
        raise HTTPException(
            status_code=400,
            detail=f"File size exceeds the maximum limit of {config.MAX_FILE_SIZE // (1024 * 1024)} MB"
        )
    try:
        file_url = upload_file_to_minio(file)
        return create_meme_db(db, meme, file_url)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.put("/{meme_id}", response_model=MemeOut)
def update_meme(meme_id: int, meme: MemeUpdate, db: Session = Depends(get_db)):
    """
    Update an existing meme.

    Args:
        meme_id: ID of the meme to update.
        meme: Meme data to update.
        db: Database session.

    Returns:
        Updated meme object.

    Raises:
        HTTPException: If the meme is not found.
    """
    db_meme = update_meme_db(db=db, meme_id=meme_id, meme=meme)
    if db_meme is None:
        raise HTTPException(status_code=404, detail=f"Meme with id {meme_id} not found")
    return db_meme


@router.delete("/{meme_id}", response_model=MemeOut)
def delete_meme(meme_id: int, db: Session = Depends(get_db)):
    """
    Delete a meme.

    Args:
        meme_id: ID of the meme to delete.
        db: Database session.

    Returns:
        Deleted meme object.

    Raises:
        HTTPException: If the meme is not found.
    """
    db_meme = delete_meme_db(db=db, meme_id=meme_id)
    if db_meme is None:
        raise HTTPException(status_code=404, detail="Meme not found")
    return db_meme
