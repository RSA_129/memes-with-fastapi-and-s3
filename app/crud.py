from typing import List, Optional

from sqlalchemy.orm import Session
from . import models, schemas


def get_meme_db(db: Session, meme_id: int) -> models.Meme:
    """
    Get a meme from the database by its ID.

    Args:
        db: The database session.
        meme_id: The ID of the meme to retrieve.

    Returns:
        The meme object if found, otherwise None.
    """
    return db.query(models.Meme).filter(models.Meme.id == meme_id).first()


def get_memes_db(db: Session, offset: int = 0, limit: int = 10) -> List[models.Meme]:
    """
    Get a list of memes from the database.

    Args:
        db: The database session.
        offset: The number of memes to skip.
        limit: The maximum number of memes to return.

    Returns:
        A list of meme objects.
    """
    return db.query(models.Meme).offset(offset).limit(limit).all()


def create_meme_db(db: Session, meme: schemas.MemeCreate, file_url: str) -> models.Meme:
    """
    Create a new meme in the database.

    Args:
        db: The database session.
        meme: The meme data to create.
        file_url: The URL of the uploaded image.

    Returns:
        The created meme object.
    """
    db_meme = models.Meme(**meme.model_dump(), image_url=file_url)
    db.add(db_meme)
    db.commit()
    db.refresh(db_meme)
    return db_meme


def update_meme_db(db: Session, meme_id: int, meme: schemas.MemeUpdate) -> Optional[models.Meme]:
    """
    Update an existing meme in the database.

    Args:
        db: The database session.
        meme_id: The ID of the meme to update.
        meme: The meme data to update.

    Returns:
        The updated meme object if found, otherwise None.
    """
    db_meme = get_meme_db(db, meme_id)
    if db_meme:
        for key, value in meme.model_dump(exclude_none=True).items():
            setattr(db_meme, key, value)
        db.commit()
        db.refresh(db_meme)
    return db_meme


def delete_meme_db(db: Session, meme_id: int):
    """
    Delete a meme from the database.

    Args:
        db: The database session.
        meme_id: The ID of the meme to delete.

    Returns:
        The deleted meme object if found, otherwise None.
    """
    db_meme = db.query(models.Meme).filter(models.Meme.id == meme_id).first()
    if db_meme:
        db.delete(db_meme)
        db.commit()
    return db_meme
