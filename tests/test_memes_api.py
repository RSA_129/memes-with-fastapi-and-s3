import json
from io import BytesIO
from typing import Optional
from fastapi.testclient import TestClient
from pydantic import BaseModel
from sqlalchemy.orm import Session
from unittest.mock import patch
from app.api.v1.memes import PREFIX


class MemeCreateTestData(BaseModel):
    """
    Test data for creating a meme.
    """
    title: str = "Test Meme"
    description: Optional[str] = "This is a test meme"


class MemeUpdateTestData(BaseModel):
    """
    Test data for updating a meme.
    """
    title: str = "Updated Meme"


meme_create_test_data = MemeCreateTestData().model_dump()
meme_update_test_data = MemeUpdateTestData().model_dump()


def test_read_memes(client: TestClient, db: Session):
    """
    Test reading all memes.
    """
    response = client.get(f"{PREFIX}")
    assert response.status_code == 200, response.text
    assert isinstance(response.json(), list)
    assert len(response.json()) == 0


def test_read_meme(client: TestClient, db: Session):
    """
    Test reading a meme that doesn't exist.
    """
    response = client.get(f"{PREFIX}/1")
    assert response.status_code == 404, response.text
    assert response.json() == {"detail": "Meme with id 1 not found"}


def test_create_meme(client: TestClient, db: Session):
    """
    Test creating a new meme.
    """
    with patch('app.api.v1.memes.upload_file_to_minio', return_value='http://minio.test/meme.jpg'):
        file = BytesIO(b"test file content")
        file.name = "test.jpg"
        response = client.post(
            f"{PREFIX}",
            data={'meme': json.dumps(meme_create_test_data)},
            files={"file": (file.name, file, 'image/jpeg')},
        )
    assert response.status_code == 200, response.text
    assert response.json()["id"] == 1
    assert response.json()["title"] == meme_create_test_data["title"]
    assert response.json()["description"] == meme_create_test_data["description"]
    assert "image_url" in response.json()


def test_update_meme(client: TestClient, db: Session):
    """
    Test updating a meme.
    """
    response = client.put(f"{PREFIX}/1", json=meme_update_test_data)
    assert response.status_code == 200, response.text
    assert response.json()["title"] == "Updated Meme"


def test_delete_meme(client: TestClient, db: Session):
    """
    Test deleting a meme.
    """
    response = client.delete(f"{PREFIX}/1")
    assert response.status_code == 200
    assert response.json()['title'] == meme_update_test_data['title']
    assert response.json()['description'] == meme_create_test_data['description']

    response = client.delete(f"{PREFIX}/1")
    assert response.status_code == 404
    assert response.json() == {"detail": "Meme not found"}

    response = client.get(f"{PREFIX}/1")
    assert response.status_code == 404
    assert response.json() == {"detail": "Meme with id 1 not found"}
