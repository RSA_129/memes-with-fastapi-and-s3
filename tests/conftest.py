import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.pool import StaticPool

from app.database import Base
from app.dependencies import get_db
from app.main import app

SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}, poolclass=StaticPool
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture(scope="session", autouse=True)
def db_session():
    """
    Sets up a database session for testing.
    Creates all tables before each test session and drops them after.
    """
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    db = TestingSessionLocal()
    try:
        yield db
        Base.metadata.drop_all(bind=engine)
    finally:
        db.close()


@pytest.fixture(scope="function")
def db(db_session: Session):
    """
    Provides a database session for each test function.
    Rolls back the transaction after each test to ensure a clean state.
    """
    db_session.begin()
    yield db_session
    db_session.rollback()


@pytest.fixture(scope="session")
def client(db_session: Session):
    """
    Sets up a test client with overridden get_db.
    """

    def override_get_db():
        try:
            yield db_session
        finally:
            db_session.close()

    app.dependency_overrides[get_db] = override_get_db
    with TestClient(app) as c:
        yield c
