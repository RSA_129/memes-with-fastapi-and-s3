# Memes API with FastAPI and MinIO

This project implements a RESTful API for managing memes using FastAPI, SQLAlchemy, and MinIO S3 for image storage.

## Features

- Create new memes with title, description, and image upload.
- Retrieve a list of memes with pagination.
- Get details of a specific meme by ID.
- Update meme information (title, description).
- Delete a meme.

## Installation

### Docker Installation

1. Install Docker and Docker compose.

2. Create a `.env` file in the root directory and add the following environment variables:

```
POSTGRES_USER=your_postgres_user
POSTGRES_PASSWORD=your_postgres_password
POSTGRES_DB=your_postgres_db

MINIO_ROOT_USER=your_minio_root_user
MINIO_ROOT_PASSWORD=your_minio_root_password
MINIO_BUCKET_NAME=your_minio_bucket_name
```

3. Build and start the Docker containers:

```bash
docker-compose up --build
```

### Local Installation

1. Create and activate a virtual environment:

```bash
python3.9 -m venv .venv
source .venv/bin/activate
```

2. Create new database in PostgreSQL.

3. Install the project dependencies:

```bash
pip install -r requirements.txt
```

4. Create a `.env` file in the root directory and add the following environment variables (as in docker installation but also needs hosts):

```
POSTGRES_HOST=your_postgres_host
POSTGRES_USER=your_postgres_user
POSTGRES_PASSWORD=your_postgres_password
POSTGRES_DB=your_postgres_db

MINIO_HOST=your_minio_host
MINIO_ROOT_USER=your_minio_root_user
MINIO_ROOT_PASSWORD=your_minio_root_password
MINIO_BUCKET_NAME=your_minio_bucket_name
```

5. Apply database migrations:

```bash
alembic upgrade head
```

6. Start the FastAPI server:

```bash
uvicorn app.main:app --reload
```

## Usage

The API will be accessible at `http://localhost:8000/api/v1/memes` <br/>
OpenAPI specification can be opened at `http://localhost:8000/docs` <br/>

![screenshot](images_repo/img.png)

### Endpoints

| Method | Endpoint                  | Description |
|---|---------------------------|---|
| GET | `/api/v1/memes`           | Get a list of memes. |
| GET | `/api/v1/memes/{meme_id}` | Get a specific meme by ID. |
| POST | `/api/v1/memes`                       | Create a new meme. |
| PUT | `/api/v1/memes/{meme_id}`              | Update an existing meme. |
| DELETE | `/api/v1/memes/{meme_id}`              | Delete a meme. |

### Example using api

To create a new meme, send a POST request to `/api/v1/memes` with the following:

- **Header:**
    - `Content-Type: multipart/form-data`
- **Body:**
    - `meme`: JSON string containing the meme data (title, description).
    - `file`: Image file to upload.

**Example using curl:**

```bash
curl -X POST \
  -H "Content-Type: multipart/form-data" \
  -F 'meme={"title": "My Meme", "description": "This is my meme."}' \
  -F 'file=@path/to/your/image.jpg' \
  http://localhost:8000/api/v1/memes
```

## Testing

Run tests using pytest:

```bash
pytest
```


